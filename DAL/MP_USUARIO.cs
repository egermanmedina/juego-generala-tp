﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_USUARIO
    {
        private Acceso acceso = new Acceso();

        public void Insertar(BE.Usuario usuario)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@usu", usuario.NombreUsuario));
            parameters.Add(acceso.CrearParametro("@nomcompleto", usuario.Nombrecompleto));
            parameters.Add(acceso.CrearParametro("@clave", usuario.Clave));
            acceso.Escribir("USUARIO_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void Editar(BE.Usuario usuario)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@id", usuario.Id));
            parameters.Add(acceso.CrearParametro("@nomcompleto", usuario.Nombrecompleto));
            parameters.Add(acceso.CrearParametro("@clave", usuario.Clave));
            acceso.Escribir("USUARIO_EDITAR", parameters);

            acceso.Cerrar();
        }



        public List<BE.Usuario> Listar()
        {
            List<BE.Usuario> lista = new List<BE.Usuario>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("USUARIO_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Usuario u = new BE.Usuario();
                u.Id = int.Parse(registro[0].ToString());
                u.NombreUsuario = registro[1].ToString();
                u.Nombrecompleto = registro[2].ToString();
                u.Clave = registro[3].ToString();


                lista.Add(u);
            }
            return lista;
        }

    }
}
