﻿namespace Presentacion
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textNombre1 = new System.Windows.Forms.TextBox();
            this.textNombre2 = new System.Windows.Forms.TextBox();
            this.textdados1J1 = new System.Windows.Forms.TextBox();
            this.textdado2J1 = new System.Windows.Forms.TextBox();
            this.textdados3J1 = new System.Windows.Forms.TextBox();
            this.textdado4J1 = new System.Windows.Forms.TextBox();
            this.textdado5J1 = new System.Windows.Forms.TextBox();
            this.textdado6J1 = new System.Windows.Forms.TextBox();
            this.textEcaleraJ1 = new System.Windows.Forms.TextBox();
            this.textFullJ1 = new System.Windows.Forms.TextBox();
            this.textPokerJ1 = new System.Windows.Forms.TextBox();
            this.textGeneralaJ1 = new System.Windows.Forms.TextBox();
            this.textPuntJ1 = new System.Windows.Forms.TextBox();
            this.textPuntJ2 = new System.Windows.Forms.TextBox();
            this.textGeneralaJ2 = new System.Windows.Forms.TextBox();
            this.textPokerJ2 = new System.Windows.Forms.TextBox();
            this.textFullJ2 = new System.Windows.Forms.TextBox();
            this.textEcaleraJ2 = new System.Windows.Forms.TextBox();
            this.textdado6J2 = new System.Windows.Forms.TextBox();
            this.textdado5J2 = new System.Windows.Forms.TextBox();
            this.textdado4J2 = new System.Windows.Forms.TextBox();
            this.textdados3J2 = new System.Windows.Forms.TextBox();
            this.textdado2J2 = new System.Windows.Forms.TextBox();
            this.textdados1J2 = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textRonda = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.dados6 = new BLL.Dados();
            this.dados5 = new BLL.Dados();
            this.dados4 = new BLL.Dados();
            this.dados3 = new BLL.Dados();
            this.dados2 = new BLL.Dados();
            this.dados1 = new BLL.Dados();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(181, 405);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(139, 43);
            this.button1.TabIndex = 6;
            this.button1.Text = "Tirar Dados";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(644, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "CATEGORIAS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(904, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "JUGADORES";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(814, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "JUGADOR UNO";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(978, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "JUGADOR DOS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(644, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(644, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(644, 223);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(644, 341);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "6";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(644, 299);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "5";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(644, 261);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "4";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(644, 542);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "PUNTUACION";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(644, 542);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 13);
            this.label12.TabIndex = 21;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(644, 504);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "Generala";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(644, 466);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "Poker";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(644, 424);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(23, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Full";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(644, 386);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "Escalera";
            // 
            // textNombre1
            // 
            this.textNombre1.Enabled = false;
            this.textNombre1.Location = new System.Drawing.Point(748, 91);
            this.textNombre1.Name = "textNombre1";
            this.textNombre1.Size = new System.Drawing.Size(152, 20);
            this.textNombre1.TabIndex = 23;
            // 
            // textNombre2
            // 
            this.textNombre2.Enabled = false;
            this.textNombre2.Location = new System.Drawing.Point(950, 91);
            this.textNombre2.Name = "textNombre2";
            this.textNombre2.Size = new System.Drawing.Size(147, 20);
            this.textNombre2.TabIndex = 24;
            // 
            // textdados1J1
            // 
            this.textdados1J1.Enabled = false;
            this.textdados1J1.Location = new System.Drawing.Point(748, 136);
            this.textdados1J1.Name = "textdados1J1";
            this.textdados1J1.Size = new System.Drawing.Size(74, 20);
            this.textdados1J1.TabIndex = 27;
            this.textdados1J1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textdado2J1
            // 
            this.textdado2J1.Enabled = false;
            this.textdado2J1.Location = new System.Drawing.Point(748, 174);
            this.textdado2J1.Name = "textdado2J1";
            this.textdado2J1.Size = new System.Drawing.Size(74, 20);
            this.textdado2J1.TabIndex = 28;
            this.textdado2J1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textdados3J1
            // 
            this.textdados3J1.Enabled = false;
            this.textdados3J1.Location = new System.Drawing.Point(748, 216);
            this.textdados3J1.Name = "textdados3J1";
            this.textdados3J1.Size = new System.Drawing.Size(74, 20);
            this.textdados3J1.TabIndex = 29;
            this.textdados3J1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textdado4J1
            // 
            this.textdado4J1.Enabled = false;
            this.textdado4J1.Location = new System.Drawing.Point(748, 258);
            this.textdado4J1.Name = "textdado4J1";
            this.textdado4J1.Size = new System.Drawing.Size(74, 20);
            this.textdado4J1.TabIndex = 30;
            this.textdado4J1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textdado5J1
            // 
            this.textdado5J1.Enabled = false;
            this.textdado5J1.Location = new System.Drawing.Point(748, 299);
            this.textdado5J1.Name = "textdado5J1";
            this.textdado5J1.Size = new System.Drawing.Size(74, 20);
            this.textdado5J1.TabIndex = 31;
            this.textdado5J1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textdado6J1
            // 
            this.textdado6J1.Enabled = false;
            this.textdado6J1.Location = new System.Drawing.Point(748, 341);
            this.textdado6J1.Name = "textdado6J1";
            this.textdado6J1.Size = new System.Drawing.Size(74, 20);
            this.textdado6J1.TabIndex = 32;
            this.textdado6J1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textEcaleraJ1
            // 
            this.textEcaleraJ1.Enabled = false;
            this.textEcaleraJ1.Location = new System.Drawing.Point(748, 383);
            this.textEcaleraJ1.Name = "textEcaleraJ1";
            this.textEcaleraJ1.Size = new System.Drawing.Size(74, 20);
            this.textEcaleraJ1.TabIndex = 33;
            this.textEcaleraJ1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textFullJ1
            // 
            this.textFullJ1.Enabled = false;
            this.textFullJ1.Location = new System.Drawing.Point(748, 424);
            this.textFullJ1.Name = "textFullJ1";
            this.textFullJ1.Size = new System.Drawing.Size(74, 20);
            this.textFullJ1.TabIndex = 34;
            this.textFullJ1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textPokerJ1
            // 
            this.textPokerJ1.Enabled = false;
            this.textPokerJ1.Location = new System.Drawing.Point(748, 466);
            this.textPokerJ1.Name = "textPokerJ1";
            this.textPokerJ1.Size = new System.Drawing.Size(74, 20);
            this.textPokerJ1.TabIndex = 35;
            this.textPokerJ1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textGeneralaJ1
            // 
            this.textGeneralaJ1.Enabled = false;
            this.textGeneralaJ1.Location = new System.Drawing.Point(748, 501);
            this.textGeneralaJ1.Name = "textGeneralaJ1";
            this.textGeneralaJ1.Size = new System.Drawing.Size(74, 20);
            this.textGeneralaJ1.TabIndex = 36;
            this.textGeneralaJ1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textPuntJ1
            // 
            this.textPuntJ1.Enabled = false;
            this.textPuntJ1.Location = new System.Drawing.Point(748, 535);
            this.textPuntJ1.Name = "textPuntJ1";
            this.textPuntJ1.Size = new System.Drawing.Size(109, 20);
            this.textPuntJ1.TabIndex = 37;
            this.textPuntJ1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textPuntJ2
            // 
            this.textPuntJ2.Enabled = false;
            this.textPuntJ2.Location = new System.Drawing.Point(950, 535);
            this.textPuntJ2.Name = "textPuntJ2";
            this.textPuntJ2.Size = new System.Drawing.Size(109, 20);
            this.textPuntJ2.TabIndex = 38;
            this.textPuntJ2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textGeneralaJ2
            // 
            this.textGeneralaJ2.Enabled = false;
            this.textGeneralaJ2.Location = new System.Drawing.Point(950, 505);
            this.textGeneralaJ2.Name = "textGeneralaJ2";
            this.textGeneralaJ2.Size = new System.Drawing.Size(74, 20);
            this.textGeneralaJ2.TabIndex = 48;
            this.textGeneralaJ2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textPokerJ2
            // 
            this.textPokerJ2.Enabled = false;
            this.textPokerJ2.Location = new System.Drawing.Point(950, 470);
            this.textPokerJ2.Name = "textPokerJ2";
            this.textPokerJ2.Size = new System.Drawing.Size(74, 20);
            this.textPokerJ2.TabIndex = 47;
            this.textPokerJ2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textFullJ2
            // 
            this.textFullJ2.Enabled = false;
            this.textFullJ2.Location = new System.Drawing.Point(950, 428);
            this.textFullJ2.Name = "textFullJ2";
            this.textFullJ2.Size = new System.Drawing.Size(74, 20);
            this.textFullJ2.TabIndex = 46;
            this.textFullJ2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textEcaleraJ2
            // 
            this.textEcaleraJ2.Enabled = false;
            this.textEcaleraJ2.Location = new System.Drawing.Point(950, 387);
            this.textEcaleraJ2.Name = "textEcaleraJ2";
            this.textEcaleraJ2.Size = new System.Drawing.Size(74, 20);
            this.textEcaleraJ2.TabIndex = 45;
            this.textEcaleraJ2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textdado6J2
            // 
            this.textdado6J2.Enabled = false;
            this.textdado6J2.Location = new System.Drawing.Point(950, 345);
            this.textdado6J2.Name = "textdado6J2";
            this.textdado6J2.Size = new System.Drawing.Size(74, 20);
            this.textdado6J2.TabIndex = 44;
            this.textdado6J2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textdado5J2
            // 
            this.textdado5J2.Enabled = false;
            this.textdado5J2.Location = new System.Drawing.Point(950, 303);
            this.textdado5J2.Name = "textdado5J2";
            this.textdado5J2.Size = new System.Drawing.Size(74, 20);
            this.textdado5J2.TabIndex = 43;
            this.textdado5J2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textdado4J2
            // 
            this.textdado4J2.Enabled = false;
            this.textdado4J2.Location = new System.Drawing.Point(950, 262);
            this.textdado4J2.Name = "textdado4J2";
            this.textdado4J2.Size = new System.Drawing.Size(74, 20);
            this.textdado4J2.TabIndex = 42;
            this.textdado4J2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textdados3J2
            // 
            this.textdados3J2.Enabled = false;
            this.textdados3J2.Location = new System.Drawing.Point(950, 220);
            this.textdados3J2.Name = "textdados3J2";
            this.textdados3J2.Size = new System.Drawing.Size(74, 20);
            this.textdados3J2.TabIndex = 41;
            this.textdados3J2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textdado2J2
            // 
            this.textdado2J2.Enabled = false;
            this.textdado2J2.Location = new System.Drawing.Point(950, 178);
            this.textdado2J2.Name = "textdado2J2";
            this.textdado2J2.Size = new System.Drawing.Size(74, 20);
            this.textdado2J2.TabIndex = 40;
            this.textdado2J2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textdados1J2
            // 
            this.textdados1J2.Enabled = false;
            this.textdados1J2.Location = new System.Drawing.Point(950, 140);
            this.textdados1J2.Name = "textdados1J2";
            this.textdados1J2.Size = new System.Drawing.Size(74, 20);
            this.textdados1J2.TabIndex = 39;
            this.textdados1J2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(74, 200);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(64, 17);
            this.checkBox1.TabIndex = 49;
            this.checkBox1.Text = "Guardar";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(195, 200);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(64, 17);
            this.checkBox2.TabIndex = 50;
            this.checkBox2.Text = "Guardar";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(328, 203);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(64, 17);
            this.checkBox3.TabIndex = 51;
            this.checkBox3.Text = "Guardar";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(74, 348);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(64, 17);
            this.checkBox4.TabIndex = 52;
            this.checkBox4.Text = "Guardar";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(195, 348);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(64, 17);
            this.checkBox5.TabIndex = 53;
            this.checkBox5.Text = "Guardar";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(328, 348);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(64, 17);
            this.checkBox6.TabIndex = 54;
            this.checkBox6.Text = "Guardar";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(647, 597);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(139, 43);
            this.button2.TabIndex = 55;
            this.button2.Text = "Sumar Puntajes";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(950, 597);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(139, 43);
            this.button3.TabIndex = 56;
            this.button3.Text = "Guardar Partida";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textRonda
            // 
            this.textRonda.Enabled = false;
            this.textRonda.Location = new System.Drawing.Point(191, 42);
            this.textRonda.Name = "textRonda";
            this.textRonda.Size = new System.Drawing.Size(74, 20);
            this.textRonda.TabIndex = 57;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(136, 45);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 13);
            this.label18.TabIndex = 58;
            this.label18.Text = " RONDA";
            // 
            // dados6
            // 
            this.dados6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.dados6.Location = new System.Drawing.Point(328, 257);
            this.dados6.Name = "dados6";
            this.dados6.Size = new System.Drawing.Size(86, 82);
            this.dados6.TabIndex = 5;
            // 
            // dados5
            // 
            this.dados5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.dados5.Location = new System.Drawing.Point(195, 257);
            this.dados5.Name = "dados5";
            this.dados5.Size = new System.Drawing.Size(86, 82);
            this.dados5.TabIndex = 4;
            // 
            // dados4
            // 
            this.dados4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.dados4.Location = new System.Drawing.Point(74, 257);
            this.dados4.Name = "dados4";
            this.dados4.Size = new System.Drawing.Size(86, 82);
            this.dados4.TabIndex = 3;
            // 
            // dados3
            // 
            this.dados3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.dados3.Location = new System.Drawing.Point(328, 112);
            this.dados3.Name = "dados3";
            this.dados3.Size = new System.Drawing.Size(86, 82);
            this.dados3.TabIndex = 2;
            // 
            // dados2
            // 
            this.dados2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.dados2.Location = new System.Drawing.Point(195, 112);
            this.dados2.Name = "dados2";
            this.dados2.Size = new System.Drawing.Size(86, 82);
            this.dados2.TabIndex = 1;
            // 
            // dados1
            // 
            this.dados1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.dados1.Location = new System.Drawing.Point(74, 112);
            this.dados1.Name = "dados1";
            this.dados1.Size = new System.Drawing.Size(86, 82);
            this.dados1.TabIndex = 0;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 705);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.textRonda);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.checkBox6);
            this.Controls.Add(this.checkBox5);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.textGeneralaJ2);
            this.Controls.Add(this.textPokerJ2);
            this.Controls.Add(this.textFullJ2);
            this.Controls.Add(this.textEcaleraJ2);
            this.Controls.Add(this.textdado6J2);
            this.Controls.Add(this.textdado5J2);
            this.Controls.Add(this.textdado4J2);
            this.Controls.Add(this.textdados3J2);
            this.Controls.Add(this.textdado2J2);
            this.Controls.Add(this.textdados1J2);
            this.Controls.Add(this.textPuntJ2);
            this.Controls.Add(this.textPuntJ1);
            this.Controls.Add(this.textGeneralaJ1);
            this.Controls.Add(this.textPokerJ1);
            this.Controls.Add(this.textFullJ1);
            this.Controls.Add(this.textEcaleraJ1);
            this.Controls.Add(this.textdado6J1);
            this.Controls.Add(this.textdado5J1);
            this.Controls.Add(this.textdado4J1);
            this.Controls.Add(this.textdados3J1);
            this.Controls.Add(this.textdado2J1);
            this.Controls.Add(this.textdados1J1);
            this.Controls.Add(this.textNombre2);
            this.Controls.Add(this.textNombre1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dados6);
            this.Controls.Add(this.dados5);
            this.Controls.Add(this.dados4);
            this.Controls.Add(this.dados3);
            this.Controls.Add(this.dados2);
            this.Controls.Add(this.dados1);
            this.Name = "Form4";
            this.Text = "Form4";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form4_Load);
            this.Shown += new System.EventHandler(this.Form4_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textEcaleraJ1;
        private System.Windows.Forms.TextBox textFullJ1;
        private System.Windows.Forms.TextBox textPokerJ1;
        private System.Windows.Forms.TextBox textGeneralaJ1;
        private System.Windows.Forms.TextBox textPuntJ1;
        private System.Windows.Forms.TextBox textPuntJ2;
        private System.Windows.Forms.TextBox textGeneralaJ2;
        private System.Windows.Forms.TextBox textPokerJ2;
        private System.Windows.Forms.TextBox textFullJ2;
        private System.Windows.Forms.TextBox textEcaleraJ2;
        public System.Windows.Forms.TextBox textNombre1;
        public System.Windows.Forms.TextBox textNombre2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        public BLL.Dados dados1;
        public BLL.Dados dados2;
        public BLL.Dados dados3;
        public BLL.Dados dados4;
        public BLL.Dados dados5;
        public BLL.Dados dados6;
        public System.Windows.Forms.TextBox textdados1J1;
        public System.Windows.Forms.TextBox textdado2J1;
        public System.Windows.Forms.TextBox textdados3J1;
        public System.Windows.Forms.TextBox textdado4J1;
        public System.Windows.Forms.TextBox textdado5J1;
        public System.Windows.Forms.TextBox textdado6J1;
        public System.Windows.Forms.TextBox textdado6J2;
        public System.Windows.Forms.TextBox textdado5J2;
        public System.Windows.Forms.TextBox textdado4J2;
        public System.Windows.Forms.TextBox textdados3J2;
        public System.Windows.Forms.TextBox textdado2J2;
        public System.Windows.Forms.TextBox textdados1J2;
        public System.Windows.Forms.TextBox textRonda;
        private System.Windows.Forms.Label label18;
    }
}