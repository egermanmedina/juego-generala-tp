﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Form1 : Form
    {
        BLL.Usuario gestor = new BLL.Usuario();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BE.Usuario u = new BE.Usuario();
            u.NombreUsuario = textBox1.Text;
            u.Nombrecompleto = textBox2.Text;
            u.Clave = textBox3.Text;

            gestor.Guardar(u);
            ListarUsuarios();
        }

        private void ListarUsuarios()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestor.Listar();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            BE.Usuario u = new BE.Usuario();
            u.Id = int.Parse(textBox4.Text);
            u.Nombrecompleto = textBox2.Text;
            u.Clave = textBox3.Text;

            gestor.Guardar(u);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBox3.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            textBox4.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ListarUsuarios();
        }

    }
}
