﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Usuario
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }

        }

        private string nombreusuario;

        public string NombreUsuario
        {
            get { return nombreusuario; }
            set { nombreusuario = value; }

        }

        private string nombrecompleto;

        public string Nombrecompleto
        {
            get { return nombrecompleto; }
            set { nombrecompleto = value; }

        }

        private string clave;

        public string Clave
        {
            get { return clave; }
            set { clave = value; }

        }

    }
}
