﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Threading;

namespace Generala
{

    class Dado
    {
        private int Numero;
        public Dado(int numero)
        {
            this.Numero = numero;
        }
        public int GetNumero()
        {
            return Numero;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {

            ArrayList Dados = new ArrayList();
            Dado D1 = new Dado(1);
            Dado D2 = new Dado(2);
            Dado D3 = new Dado(3);
            Dado D4 = new Dado(4);
            Dado D5 = new Dado(5);
            Dados.Add(D1);
            Dados.Add(D2);
            Dados.Add(D3);
            Dados.Add(D4);
            Dados.Add(D5);
            for (int i = 0; i < Dados.Count; i++)
            {
                Console.WriteLine("Dado {0}: {1}", i, ((Dado)Dados[i]).GetNumero());
            }
            if (Generala(Dados) == true)
            {
                Console.Write("Hiciste Generala");
            }
            Poker(Dados);
            Full(Dados);
            Escalera(Dados);
            Console.ReadKey();
        }
    
    
     static bool Generala(ArrayList Dados)
        {
            bool Generala = false;
            int contador = 1;
            for (int i = 1; i < Dados.Count; i++)
            {
                if (((Dado)Dados[i]).GetNumero() == ((Dado)Dados[0]).GetNumero())
                {
                    contador++;
                }
            }
            if (contador == 5)
            {
                Generala = true;
            }
            return Generala;
        }
        static void Poker(ArrayList Dados)
        {
            int[] numeros = new int[7];
            int i = 0;
            int poker = 0;
            int numerocon4 = 0;
            int posicion = 0;
            for (i = 0; i < Dados.Count; i++)
            {
                if (((Dado)Dados[i]).GetNumero() == 1)
                {
                    numeros[1]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 2)
                {
                    numeros[2]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 3)
                {
                    numeros[3]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 4)
                {
                    numeros[4]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 5)
                {
                    numeros[5]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 6)
                {
                    numeros[6]++;
                }
            }
            for (i = 0; i < numeros.Length; i++)
            {
                if (numeros[i] == 4)
                {
                    poker++;
                    numerocon4 = i;
                }
            }
            for (i = 0; i < Dados.Count; i++)
            {
                if (((Dado)Dados[i]).GetNumero() != numerocon4)
                {
                    posicion = i;
                }
            }
            if (poker == 1)
            {
                Console.WriteLine("Hiciste Poker");
                Console.Write("Queres tirar de nuevo?(S/N)\nOpcion:");
                switch (Console.ReadLine())
                {
                    case "S":
                        Console.Clear();
                        //Retirar dado en posicion (Lo hago en claseXD)
                        Generala(Dados);
                        break;
                    case "N":
                        Console.Clear();
                        break;
                }
            }
        }
        static void Full(ArrayList Dados)
        {
            int[] numeros = new int[7];
            int i = 0;
            int numerocon3 = 0;
            int numerocon2 = 0;
            for (i = 0; i < Dados.Count; i++)
            {
                if (((Dado)Dados[i]).GetNumero() == 1)
                {
                    numeros[1]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 2)
                {
                    numeros[2]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 3)
                {
                    numeros[3]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 4)
                {
                    numeros[4]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 5)
                {
                    numeros[5]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 6)
                {
                    numeros[6]++;
                }
            }
            for (i = 0; i < numeros.Length; i++)
            {
                if (numeros[i] == 3)
                {
                    numerocon3 = i;
                }
            }
            if (numerocon3 == 1)
            {
                for (i = 0; i < numeros.Length; i++)
                {
                    if (numeros[i] == 2)
                    {
                        numerocon2 = i;
                    }
                }
            }
            if (numerocon3 == 1 && numerocon2 != 0)
            {
                Console.WriteLine("Hiciste Full");
                Console.Write("Queres tirar de nuevo?(S/N)\nOpcion:");
                switch (Console.ReadLine())
                {
                    case "S":
                        Console.Clear();
                        //Retirar dado en posicion (Lo hago en claseXD)
                        Generala(Dados);
                        Poker(Dados);
                        break;
                    case "N":
                        Console.Clear();
                        break;
                }
            }
        }
        static void Escalera(ArrayList Dados)
        {
            int[] numeros = new int[7];
            int i = 0;
            int escalera = 0;
            for (i = 0; i < Dados.Count; i++)
            {
                if (((Dado)Dados[i]).GetNumero() == 1)
                {
                    numeros[1]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 2)
                {
                    numeros[2]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 3)
                {
                    numeros[3]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 4)
                {
                    numeros[4]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 5)
                {
                    numeros[5]++;
                }
                if (((Dado)Dados[i]).GetNumero() == 6)
                {
                    numeros[6]++;
                }
            }
            if (numeros[1] == 1 && numeros[2] == 1 && numeros[3] == 1 && numeros[4] == 1 && numeros[5] == 1)
            {
                escalera = 1;
            }
            if (numeros[2] == 1 && numeros[3] == 1 && numeros[4] == 1 && numeros[5] == 1 && numeros[6] == 1)
            {
                escalera = 1;
            }
            if (escalera == 1)
            {
                Console.WriteLine("Hiciste Escalera");
                Console.Write("Queres tirar de nuevo?(S/N)\nOpcion:");
                switch (Console.ReadLine())
                {
                    case "S":
                        Console.Clear();
                        //Retirar dado en posicion (Lo hago en claseXD)
                        Generala(Dados);
                        Poker(Dados);
                        Full(Dados);
                        break;
                    case "N":
                        Console.Clear();
                        break;
                }
            }
        }
    }
}

    
    