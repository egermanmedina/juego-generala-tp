﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Form3 : Form
    {
        

        BLL.Partida gestorPartida = new BLL.Partida();
        BLL.Usuario gestor = new BLL.Usuario();
       

        public Form3()
        {

            InitializeComponent();
            textBox3.Text = DateTime.Now.ToString("dd/MM/yyyy");
           
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            ListarUsuarios();
           
        }

        private void ListarUsuarios()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestor.Listar();
            dataGridView2.DataSource = gestor.Listar();

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox4.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            if (textBox1.Text == textBox2.Text)
            {

                MessageBox.Show("No puede seleccionar 2 jugadores iguales");
                textBox1.Text = "";
                textBox4.Text = "";
            }
           
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox2.Text = dataGridView2.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox5.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            if (textBox1.Text == textBox2.Text) 
            {
                
                MessageBox.Show("No puede seleccionar 2 jugadores iguales");
                textBox2.Text = "";
                textBox5.Text = "";
            }
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "") 
            {

                MessageBox.Show("Para iniciar la partida deben estar seleccionado ambos jugadores");
            
            }

            else 
            {



                BE.Partida p = new BE.Partida();
                p.JugadorUno = int.Parse(textBox4.Text);
                p.JugadorDos = int.Parse(textBox5.Text);
                p.Fecha = textBox3.Text;

                gestorPartida.GuardarPartida(p);

                MessageBox.Show("Partida Guardada, ya puede comenzar");

                Form4 f4 = new Form4();
                f4.textNombre1.Text = textBox1.Text;
                f4.textNombre2.Text = textBox2.Text;
                f4.Show();
                
                
                

            
            }
        }

       
       

      

    }
}
