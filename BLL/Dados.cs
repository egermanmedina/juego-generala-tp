﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BLL
{
    public partial class Dados : UserControl
    {
        public Dados()
        {
            InitializeComponent();
        }

        private void Dados_Load(object sender, EventArgs e)
        {

        }

         public void EstablecerImagen(int valor)
        {

            switch (valor)
            {
            
                case 1:
                    {

                        pictureBox1.Image = Image.FromFile("imagenes\\cara1.png ");
                        break;
                    }
                case 2:
                    {

                        pictureBox1.Image = Image.FromFile("imagenes\\cara2.png ");
                        break;
                    }

                case 3:
                    {

                        pictureBox1.Image = Image.FromFile("imagenes\\cara3.png ");
                        break;
                    }

                case 4:
                    {

                        pictureBox1.Image = Image.FromFile("imagenes\\cara4.png ");
                        break;
                    }

                case 5:
                    {

                        pictureBox1.Image = Image.FromFile("imagenes\\cara5.png ");
                        break;
                    }

                case 6:
                    {

                        pictureBox1.Image = Image.FromFile("imagenes\\cara6.png ");
                        break;
                    }

                default:
                    {
                        pictureBox1.Image = Image.FromFile("imagenes\\vaso.png ");
                        break;

                    }

            }
        }
    }
}
