﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
   public class Partida
    {
        private int numeropartida;

        public int NumeroPartida
        {
            get { return numeropartida; }
            set { numeropartida = value; }

        }

        private int jugadoruno;

        public int JugadorUno
        {
            get { return jugadoruno; }
            set { jugadoruno = value; }

        }

        private int jugadordos;

        public int JugadorDos
        {
            get { return jugadordos; }
            set { jugadordos = value; }

        }

        private string nombrejugadoruno;

        public string NombreJugadorUno
        {
            get { return nombrejugadoruno; }
            set { nombrejugadoruno = value; }

        }

        private string nombrejugadordos;

        public string NombreJugadorDos
        {
            get { return nombrejugadordos; }
            set { nombrejugadordos = value; }

        }

        private string fecha;

        public string Fecha
        {
            get { return fecha; }
            set { fecha = value; }

        }


       



    }
}
