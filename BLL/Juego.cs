﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BLL
{
    public class Juego
    {
        
       public static Random dado = new Random();

       public int TirarDado()
       {

           int numero = dado.Next(1,7);
           return numero;
       
       }

       public int GirarVaso()
       {

           int numero = 0;
           return numero;
       
       }

       public int Turno(int turno) 
       {

           if ((turno % 2) == 0) 
           {
               return 1;
           }
           else
           {
               return 2;
           }
       
       }

       public int Generala(List<int>Dados)
       {
           int[] numeros = new int[7];
           int i = 0;
           int Generala = 0 ;
           int contador = 0;
           for (i = 0; i < Dados.Count; i++)
           {
               
               if (Dados[i] == Dados[0])
               {
                   contador++;
               }
           }
           if (contador == 6)
           {
               Generala = 60;
           }
           return Generala;
       }

       public  int Poker(List<int> Dados)
       {
           int[] numeros = new int[7];
           int i = 0;
           int poker = 0;
           int numerocon4 = 0;
           for (i = 0; i < Dados.Count; i++)
           {
               if ( Dados[i] == 1)
               {
                   numeros[1]++;
               }
               if (Dados[i] == 2)
               {
                   numeros[2]++;
               }
               if (Dados[i] == 3)
               {
                   numeros[3]++;
               }
               if (Dados[i] == 4)
               {
                   numeros[4]++;
               }
               if (Dados[i] == 5)
               {
                   numeros[5]++;
               }
               if (Dados[i] == 6)
               {
                   numeros[6]++;
               }
           }
           for (i = 0; i < numeros.Length; i++)
           {
               if (numeros[i] == 4 || numeros[i] == 5)
               {
                   poker = 40;
                   numerocon4 = i;
                   return poker;
               }
               
           }

           return poker;
          
       }


       public int Full(List<int> Dados)
       {
           int[] numeros = new int[7];
           int i = 0;
           int numerocon3 = 0;
           int numerocon2 = 0;
           int full = 0;
           for (i = 0; i < Dados.Count; i++)
           {
               if ( Dados[i] == 1)
               {
                   numeros[1]++;
               }
               if ( Dados[i] == 2)
               {
                   numeros[2]++;
               }
               if ( Dados[i] == 3)
               {
                   numeros[3]++;
               }
               if ( Dados[i] == 4)
               {
                   numeros[4]++;
               }
               if ( Dados[i] == 5)
               {
                   numeros[5]++;
               }
               if ( Dados[i] == 6)
               {
                   numeros[6]++;
               }
           }
           for (i = 0; i < numeros.Length; i++)
           {
               if (numeros[i] == 3)
               {
                   numerocon3 = 1;
               }
           }
           if (numerocon3 == 1)
           {
               for (i = 0; i < numeros.Length; i++)
               {
                   if (numeros[i] == 2)
                   {
                       numerocon2 = 1;
                   }
               }
           }
           if (numerocon3 == 1 && numerocon2 != 0)
           {
               full = 30;
               return full;
               
           }

           return full;
       }

       public int Escalera(List<int> Dados)
       {
           int[] numeros = new int[7];
           int i = 0;
           int escalera = 0;
           int numerosNoRepetidos = 0;

           for (i = 0; i < Dados.Count; i++)
           {
               if (Dados[i] == 1)
               {
                   numeros[1]++;
                 
               }
               if (Dados[i] == 2)
               {
                   numeros[2]++;
               }
               if (Dados[i] == 3)
               {
                   numeros[3]++;
               }
               if (Dados[i] == 4)
               {
                   numeros[4]++;
               }
               if (Dados[i] == 5)
               {
                   numeros[5]++;
               }
               if (Dados[i] == 6)
               {
                   numeros[6]++;
               }
           }

           for (i = 0; i < numeros.Length; i++)
           {
               if (numeros[i] == 1)
               {
                   numerosNoRepetidos++;
                   if (numerosNoRepetidos == 5)
                   {
                       escalera = 20;
                       return escalera;
                   }
               }

               if (numeros[i] == 2) 
               {
                   numerosNoRepetidos++;
               }
           
           }

           if (numerosNoRepetidos == 5) 
           {
               escalera = 20;
               return escalera;
           }

           return escalera;

        }

       public int SumaNumerosCategoria(List<int> Dados, int categoria)
       {
           int resultado = 0;
           int sumaCategoria1 =0;
           int sumaCategoria2 = 0;
           int sumaCategoria3 = 0;
           int sumaCategoria4 = 0;
           int sumaCategoria5 = 0;
           int sumaCategoria6 = 0;
           int[] numeros = new int[7];
           int i = 0;
           for (i = 0; i < Dados.Count; i++)
           {
               if (Dados[i] == 1)
               {

                   sumaCategoria1 = sumaCategoria1 + 1;

               }
               if (Dados[i] == 2)
               {
                   sumaCategoria2 = sumaCategoria2 + 1;
               }
               if (Dados[i] == 3)
               {
                   sumaCategoria3 = sumaCategoria3 + 1;
               }
               if (Dados[i] == 4)
               {
                   sumaCategoria4 = sumaCategoria4 + 1;
               }
               if (Dados[i] == 5)
               {
                   sumaCategoria5 = sumaCategoria5 + 1;
               }
               if (Dados[i] == 6)
               {
                   sumaCategoria6 = sumaCategoria6 + 1;
               }
           }

           if (categoria == 1) 
           {
               resultado = categoria * sumaCategoria1; 
           }

           if (categoria == 2)
           {
               resultado = categoria * sumaCategoria2;
           }

           if (categoria == 3)
           {
               resultado = categoria * sumaCategoria3;
           }

           if (categoria == 4)
           {
               resultado = categoria * sumaCategoria4;
           }

           if (categoria == 5)
           {
               resultado = categoria * sumaCategoria5;
           }

           if (categoria == 6)
           {
               resultado = categoria * sumaCategoria6;
           }

           return resultado;
           


           
       
       }


       public int sumarPuntos(List<int> puntos)
       {
           int sumadepuntos = 0;
           int i = 0;

           for (i = 0; i < puntos.Count; i++)
           {


               sumadepuntos = sumadepuntos + puntos[i];

           }
           
           
           
           return sumadepuntos;

       }

    
    
    
    }
}
