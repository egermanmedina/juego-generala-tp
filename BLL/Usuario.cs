﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Usuario
    {
        DAL.MP_USUARIO mp = new DAL.MP_USUARIO();

        public void Guardar(BE.Usuario usuario)
        {

            if (usuario.Id == 0)
            {

                mp.Insertar(usuario);

            }

            else
            {

                mp.Editar(usuario);

            }

        }


        public List<BE.Usuario> Listar()
        {

            return mp.Listar();

        }
    }
}
