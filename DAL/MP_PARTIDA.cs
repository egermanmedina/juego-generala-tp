﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace DAL
{
  public  class MP_PARTIDA
    {
      private Acceso acceso = new Acceso();

      public void CrearPartida(BE.Partida partida)
      {
          acceso.Abrir();
          List<IDbDataParameter> parameters = new List<IDbDataParameter>();
          parameters.Add(acceso.CrearParametro("@juguno", partida.JugadorUno));
          parameters.Add(acceso.CrearParametro("@jugdos", partida.JugadorDos));
          parameters.Add(acceso.CrearParametro("@fecha", partida.Fecha));
          acceso.Escribir("PARTIDA_INSERTAR", parameters);

          acceso.Cerrar();
      }

      public List<BE.Partida> ListarPartidas()
      {
          List<BE.Partida> lista = new List<BE.Partida>();

          Acceso acceso = new Acceso();
          acceso.Abrir();
          DataTable tabla = acceso.Leer("PARTIDA_LISTAR");
          acceso.Cerrar();

          foreach (DataRow registro in tabla.Rows)
          {
              BE.Partida p = new BE.Partida();
              p.NumeroPartida = int.Parse(registro[0].ToString());
              p.NombreJugadorUno = registro[1].ToString();
              p.NombreJugadorDos = registro[2].ToString();
              p.Fecha = registro[3].ToString();


              lista.Add(p);
          }
          return lista;
      }
    }
}
