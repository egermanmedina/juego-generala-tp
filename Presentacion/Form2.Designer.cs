﻿namespace Presentacion
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altaModificacionUsuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estadisticasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.juegoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iniciarPartidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estadisticasDeJuegoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.promediosDeVictoriasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usuariosToolStripMenuItem,
            this.juegoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(785, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.altaModificacionUsuariosToolStripMenuItem,
            this.estadisticasToolStripMenuItem});
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.usuariosToolStripMenuItem.Text = "Jugadores";
            // 
            // altaModificacionUsuariosToolStripMenuItem
            // 
            this.altaModificacionUsuariosToolStripMenuItem.Name = "altaModificacionUsuariosToolStripMenuItem";
            this.altaModificacionUsuariosToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.altaModificacionUsuariosToolStripMenuItem.Text = "Alta/Modificacion Jugadores";
            this.altaModificacionUsuariosToolStripMenuItem.Click += new System.EventHandler(this.altaModificacionUsuariosToolStripMenuItem_Click);
            // 
            // estadisticasToolStripMenuItem
            // 
            this.estadisticasToolStripMenuItem.Name = "estadisticasToolStripMenuItem";
            this.estadisticasToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.estadisticasToolStripMenuItem.Text = "Estadisticas de Tiempo Jugado";
            // 
            // juegoToolStripMenuItem
            // 
            this.juegoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iniciarPartidaToolStripMenuItem,
            this.estadisticasDeJuegoToolStripMenuItem,
            this.promediosDeVictoriasToolStripMenuItem});
            this.juegoToolStripMenuItem.Name = "juegoToolStripMenuItem";
            this.juegoToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.juegoToolStripMenuItem.Text = "Juego";
            // 
            // iniciarPartidaToolStripMenuItem
            // 
            this.iniciarPartidaToolStripMenuItem.Name = "iniciarPartidaToolStripMenuItem";
            this.iniciarPartidaToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.iniciarPartidaToolStripMenuItem.Text = "Iniciar Partida";
            this.iniciarPartidaToolStripMenuItem.Click += new System.EventHandler(this.iniciarPartidaToolStripMenuItem_Click);
            // 
            // estadisticasDeJuegoToolStripMenuItem
            // 
            this.estadisticasDeJuegoToolStripMenuItem.Name = "estadisticasDeJuegoToolStripMenuItem";
            this.estadisticasDeJuegoToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.estadisticasDeJuegoToolStripMenuItem.Text = "Estadisticas de Juego";
            // 
            // promediosDeVictoriasToolStripMenuItem
            // 
            this.promediosDeVictoriasToolStripMenuItem.Name = "promediosDeVictoriasToolStripMenuItem";
            this.promediosDeVictoriasToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.promediosDeVictoriasToolStripMenuItem.Text = "Promedios de Victorias";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 486);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form2";
            this.Text = "Inicio";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altaModificacionUsuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estadisticasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem juegoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iniciarPartidaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estadisticasDeJuegoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem promediosDeVictoriasToolStripMenuItem;
    }
}