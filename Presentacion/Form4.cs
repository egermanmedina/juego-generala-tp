﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        int tiro = 0;
        int numeroDado1;
        int numeroDado2;
        int numeroDado3;
        int numeroDado4;
        int numeroDado5;
        int numeroDado6;
        int turno = 1;
        int NumeroRonda = 1;
       public int resultadoSeleccionCategoria;

       
      

        BLL.Juego gestorDado = new BLL.Juego();

        private void Form4_Load(object sender, EventArgs e)
        {

            button2.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (NumeroRonda == 11) 
            {

                MessageBox.Show("Se terminaron las rondas, a continuacion sume los puntos para ver el ganador");
                button1.Enabled = false;
                button2.Enabled = true;
               
            }
            
            if (tiro < 3)
            {
                tiro++;

                if (checkBox1.Checked == false)
                {

                    int resultado = gestorDado.TirarDado();
                    dados1.EstablecerImagen(resultado);
                    numeroDado1 = resultado;

                }

                if (checkBox2.Checked == false)
                {
                    int resultado = gestorDado.TirarDado();
                    dados2.EstablecerImagen(resultado);
                    numeroDado2 = resultado;

                }

                if (checkBox3.Checked == false)
                {

                    int resultado = gestorDado.TirarDado();
                    dados3.EstablecerImagen(resultado);
                    numeroDado3 = resultado;
                }

                if (checkBox4.Checked == false)
                {
                    int resultado = gestorDado.TirarDado();
                    dados4.EstablecerImagen(resultado);
                    numeroDado4 = resultado;

                }

                if (checkBox5.Checked == false)
                {
                    int resultado = gestorDado.TirarDado();
                    dados5.EstablecerImagen(resultado);
                    numeroDado5 = resultado;

                }

                if (checkBox6.Checked == false)
                {
                    int resultado = gestorDado.TirarDado();
                    dados6.EstablecerImagen(resultado);
                    numeroDado6 = resultado;
                }

                if (checkBox1.Checked == true && checkBox2.Checked == true && checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == true && checkBox6.Checked == true)
                {


                    turno++;
                    int jugador = gestorDado.Turno(turno);                    
                    List<int> dados = new List<int>();
                    dados.Add(numeroDado1);
                    dados.Add(numeroDado2);
                    dados.Add(numeroDado3);
                    dados.Add(numeroDado4);
                    dados.Add(numeroDado5);
                    dados.Add(numeroDado6);

                    if(jugador == 1)
                    {
                        
                        
                        if ( textGeneralaJ1.Text == "0") 
                        {

                            
                            textGeneralaJ1.Text = gestorDado.Generala(dados).ToString();
                        
                        }

                        if ( textPokerJ1.Text == "0")
                        {


                            textPokerJ1.Text = gestorDado.Poker(dados).ToString();

                        }

                        if ( textFullJ1.Text == "0")
                        {


                            textFullJ1.Text = gestorDado.Full(dados).ToString();

                        }

                        if ( textEcaleraJ1.Text == "0")
                        {


                            textEcaleraJ1.Text = gestorDado.Escalera(dados).ToString();

                        }
                       
                   
                        MessageBox.Show("Cambio de Turno, ahora le toca al jugador " + textNombre2.Text);
                       
                    
                    }
                    else
                    {

                        if ( textGeneralaJ2.Text == "0")
                        {


                            textGeneralaJ2.Text = gestorDado.Generala(dados).ToString();

                        }

                        if ( textPokerJ2.Text == "0")
                        {


                            textPokerJ2.Text = gestorDado.Poker(dados).ToString();

                        }

                        if ( textFullJ2.Text == "0")
                        {


                            textFullJ2.Text = gestorDado.Full(dados).ToString();

                        }

                        if ( textEcaleraJ2.Text == "0")
                        {


                            textEcaleraJ2.Text = gestorDado.Escalera(dados).ToString();

                        }
                       
                         
                       

                        MessageBox.Show("Cambio de Turno, ahora le toca al otro jugador "+ textNombre1.Text);
                        textRonda.Text = NumeroRonda++.ToString();
                        
                    }

                   

                    
                tiro = 0;
                
                int numeroDado = gestorDado.GirarVaso();
                dados1.EstablecerImagen(numeroDado);
                dados2.EstablecerImagen(numeroDado);
                dados3.EstablecerImagen(numeroDado);
                dados4.EstablecerImagen(numeroDado);
                dados5.EstablecerImagen(numeroDado);
                dados6.EstablecerImagen(numeroDado);

                checkBox1.Checked = false;
                checkBox2.Checked = false;
                checkBox3.Checked = false;
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                checkBox6.Checked = false;
                
                
                }

            }

            else
            {
                turno++;
                int jugador = gestorDado.Turno(turno);
                int bandera = 0;
               List<int> dados = new List<int>();
                
                dados.Add(numeroDado1);
                dados.Add(numeroDado2);
                dados.Add(numeroDado3);
                dados.Add(numeroDado4);
                dados.Add(numeroDado5);
                dados.Add(numeroDado6);

                if (jugador == 1)
                {
                    

                    if (textGeneralaJ1.Text == "0")
                    {


                        textGeneralaJ1.Text = gestorDado.Generala(dados).ToString();
                        if (int.Parse(textGeneralaJ1.Text) > 1) 
                        {
                            bandera = 1;
                        } 
                    }

                    if (textPokerJ1.Text == "0")
                    {


                        textPokerJ1.Text = gestorDado.Poker(dados).ToString();
                        if (int.Parse(textPokerJ1.Text) > 1)
                        {
                            bandera = 1;
                        } 

                    }

                    if (textFullJ1.Text == "0")
                    {


                        textFullJ1.Text = gestorDado.Full(dados).ToString();
                        if (int.Parse(textFullJ1.Text) > 1)
                        {
                            bandera = 1;
                        } 

                    }

                    if (textEcaleraJ1.Text == "0")
                    {


                        textEcaleraJ1.Text = gestorDado.Escalera(dados).ToString();
                        if (int.Parse(textEcaleraJ1.Text) > 1)
                        {
                            bandera = 1;
                        } 
                    }


                    //int resultadoGenerala = gestorDado.Generala(dados);
                    //int resultadoPoker = gestorDado.Poker(dados);
                    //int resultadoFull = gestorDado.Full(dados);
                    //int resultadoEscalera = gestorDado.Escalera(dados);



                    if (bandera == 0) 
                    {

                       MessageBox.Show("Elegir Categoria de Numeros para asignar puntuacion");
                       Form5 form = new Form5();
                       form.ShowDialog();
                       resultadoSeleccionCategoria  = int.Parse(form.comboBox1.Text);

                       switch (resultadoSeleccionCategoria) 
                       {
                       
                           case 1:
                               {

                                   textdados1J1.Text = gestorDado.SumaNumerosCategoria(dados, resultadoSeleccionCategoria).ToString();
                                   textdados1J1.Enabled = false; 
                                   break;
                               }

                           case 2:
                               {

                                   textdado2J1.Text = gestorDado.SumaNumerosCategoria(dados, resultadoSeleccionCategoria).ToString();
                                   break;
                               }

                           case 3:
                               {

                                   textdados3J1.Text = gestorDado.SumaNumerosCategoria(dados, resultadoSeleccionCategoria).ToString();
                                   break;
                               }

                           case 4:
                               {

                                   textdado4J1.Text = gestorDado.SumaNumerosCategoria(dados, resultadoSeleccionCategoria).ToString();
                                   break;
                               }

                           case 5:
                               {

                                   textdado5J1.Text = gestorDado.SumaNumerosCategoria(dados, resultadoSeleccionCategoria).ToString();
                                   break;
                               }

                           case 6:
                               {

                                   textdado6J1.Text = gestorDado.SumaNumerosCategoria(dados, resultadoSeleccionCategoria).ToString();
                                   break;
                               }
                       
                       }
                        
                        
                    
                    }
                    
                    
                    
                    MessageBox.Show("Cambio de Turno, ahora le toca a  " + textNombre2.Text);
                     

                }
                else
                {

                    if (textGeneralaJ2.Text == "0")
                    {


                        textGeneralaJ2.Text = gestorDado.Generala(dados).ToString();
                        if (int.Parse(textGeneralaJ2.Text) > 1)
                        {
                            bandera = 1;
                        } 
                    }

                    if (textPokerJ2.Text == "0")
                    {


                        textPokerJ2.Text = gestorDado.Poker(dados).ToString();
                        if (int.Parse(textPokerJ2.Text) > 1)
                        {
                            bandera = 1;
                        } 
                    }

                    if (textFullJ2.Text == "0")
                    {


                        textFullJ2.Text = gestorDado.Full(dados).ToString();
                        if (int.Parse(textFullJ2.Text) > 1)
                        {
                            bandera = 1;
                        } 
                    }

                    if (textEcaleraJ2.Text == "0")
                    {


                        textEcaleraJ2.Text = gestorDado.Escalera(dados).ToString();
                        if (int.Parse(textEcaleraJ2.Text) > 1)
                        {
                            bandera = 1;
                        } 
                    }



                    //int resultadoGenerala = gestorDado.Generala(dados);           
                    //int resultadoPoker = gestorDado.Poker(dados);
                    //int resultadoFull = gestorDado.Full(dados);
                    //int resultadoEscalera = gestorDado.Escalera(dados);


                    if ( bandera == 0 ) 
                    {
                        if (int.Parse(textdados1J1.Text) == 0 || int.Parse(textdado2J1.Text) == 0 || int.Parse(textdados3J1.Text) == 0 ||  int.Parse(textdado4J1.Text)== 0 ||  int.Parse(textdado5J1.Text) == 0 ||  int.Parse(textdado6J1.Text) == 0 )
                        { 
                        MessageBox.Show("Elegir Categoria de Numeros para asignar puntuacion");
                        Form5 form = new Form5();
                        form.ShowDialog();
                        resultadoSeleccionCategoria = int.Parse(form.comboBox1.Text);

                        switch (resultadoSeleccionCategoria)
                        {

                            case 1:
                                {

                                    textdados1J2.Text = gestorDado.SumaNumerosCategoria(dados, resultadoSeleccionCategoria).ToString();
                                    
                                    break;
                                }

                            case 2:
                                {

                                    textdado2J2.Text = gestorDado.SumaNumerosCategoria(dados, resultadoSeleccionCategoria).ToString();
                                    
                                    break;
                                }

                            case 3:
                                {

                                    textdados3J2.Text = gestorDado.SumaNumerosCategoria(dados, resultadoSeleccionCategoria).ToString();
                                   
                                    break;
                                }

                            case 4:
                                {

                                    textdado4J2.Text = gestorDado.SumaNumerosCategoria(dados, resultadoSeleccionCategoria).ToString();
                                    
                                    break;
                                }

                            case 5:
                                {

                                    textdado5J2.Text = gestorDado.SumaNumerosCategoria(dados, resultadoSeleccionCategoria).ToString();
                                   
                                    break;
                                }

                            case 6:
                                {

                                    textdado6J2.Text = gestorDado.SumaNumerosCategoria(dados, resultadoSeleccionCategoria).ToString();
                                    
                                    break;
                                }

                        }

                        }

                       
                      


                    }




                    MessageBox.Show("Cambio de Turno, ahora le toca a " + textNombre1.Text);
                    textRonda.Text = NumeroRonda++.ToString();
                    
                      
                }

                
                tiro = 0;
                
                int numeroDado = gestorDado.GirarVaso();
                dados1.EstablecerImagen(numeroDado);
                dados2.EstablecerImagen(numeroDado);
                dados3.EstablecerImagen(numeroDado);
                dados4.EstablecerImagen(numeroDado);
                dados5.EstablecerImagen(numeroDado);
                dados6.EstablecerImagen(numeroDado);

                checkBox1.Checked = false;
                checkBox2.Checked = false;
                checkBox3.Checked = false;
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                checkBox6.Checked = false;
                
                

            }

        }

        private void Form4_Shown(object sender, EventArgs e)
        {
            int numeroDado = gestorDado.GirarVaso();
            dados1.EstablecerImagen(numeroDado);
            dados2.EstablecerImagen(numeroDado);
            dados3.EstablecerImagen(numeroDado);
            dados4.EstablecerImagen(numeroDado);
            dados5.EstablecerImagen(numeroDado);
            dados6.EstablecerImagen(numeroDado);

            // Campos Jugador 1
            textdados1J1.Text = "0";
            textdado2J1.Text = "0";
            textdados3J1.Text = "0";
            textdado4J1.Text = "0";
            textdado5J1.Text = "0";
            textdado6J1.Text = "0";
            textGeneralaJ1.Text = "0";
            textFullJ1.Text = "0";
            textPokerJ1.Text = "0";
            textEcaleraJ1.Text = "0";
            

            // Campos Jugador 2
            textdados1J2.Text = "0";
            textdado2J2.Text = "0";
            textdados3J2.Text = "0";
            textdado4J2.Text = "0";
            textdado5J2.Text = "0";
            textdado6J2.Text = "0";
            textGeneralaJ2.Text = "0";
            textFullJ2.Text = "0";
            textPokerJ2.Text = "0";
            textEcaleraJ2.Text = "0";


        }

        private void button2_Click(object sender, EventArgs e)
        {

            // Suma de Puntos Jugador 1
            
            List<int> puntosJug1 = new List<int>();

            puntosJug1.Add(int.Parse(textdados1J1.Text));
            puntosJug1.Add(int.Parse(textdado2J1.Text));
            puntosJug1.Add(int.Parse(textdados3J1.Text));
            puntosJug1.Add(int.Parse(textdado4J1.Text));
            puntosJug1.Add(int.Parse(textdado5J1.Text));
            puntosJug1.Add(int.Parse(textdado6J1.Text));
            puntosJug1.Add(int.Parse(textEcaleraJ1.Text));
            puntosJug1.Add(int.Parse(textFullJ1.Text));
            puntosJug1.Add(int.Parse(textGeneralaJ1.Text));
            puntosJug1.Add(int.Parse(textPokerJ1.Text));

            textPuntJ1.Text = gestorDado.sumarPuntos(puntosJug1).ToString();

            // Suma de Puntos Jugador 2

            List<int> puntosJug2 = new List<int>();

            puntosJug2.Add(int.Parse(textdados1J2.Text));
            puntosJug2.Add(int.Parse(textdado2J2.Text));
            puntosJug2.Add(int.Parse(textdados3J2.Text));
            puntosJug2.Add(int.Parse(textdado4J2.Text));
            puntosJug2.Add(int.Parse(textdado5J2.Text));
            puntosJug2.Add(int.Parse(textdado6J2.Text));
            puntosJug2.Add(int.Parse(textEcaleraJ2.Text));
            puntosJug2.Add(int.Parse(textFullJ2.Text));
            puntosJug2.Add(int.Parse(textGeneralaJ2.Text));
            puntosJug2.Add(int.Parse(textPokerJ2.Text));

            textPuntJ2.Text = gestorDado.sumarPuntos(puntosJug2).ToString();

            if (int.Parse(textPuntJ1.Text) > int.Parse(textPuntJ2.Text)) 
            {

                MessageBox.Show("El ganador del Juego es :  " + textNombre1.Text);
            
            }

            if (int.Parse(textPuntJ1.Text) < int.Parse(textPuntJ2.Text))
            {

                MessageBox.Show("El ganador del Juego es :  " + textNombre2.Text);

            }

            if (int.Parse(textPuntJ1.Text) == int.Parse(textPuntJ2.Text))
            {

                MessageBox.Show(" El partido termino empatado");

            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

      

    }

}
